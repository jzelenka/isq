#!/usr/bin/env python3
from matplotlib.backends.backend_qt5agg import\
        FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from matplotlib.patches import Rectangle as rect
from PyQt5 import QtGui
from PyQt5 import QtWidgets
from PyQt5 import QtCore
import numpy as np
import pandas as pd
import sys


def load_file(fn):
    a = np.asarray(pd.read_table(
                   fn, delim_whitespace=True, skiprows=43, header=None,
                   names=("Time (min)", "step (ignore)", "Pressure (bar)"))
                   [['Time (min)','Pressure (bar)']]).T

    return a[0], a[1]


def populate(datas, plot, style):
    #plot.plot(datas[0], datas[1], color=(0,0.9,0,1))
    plot.plot(datas[0], datas[1], linewidth=0.8, **style)
    return 0


def main(pathprefix, files, styles, outname, index):
    app = QtWidgets.QApplication(sys.argv)
    graph = Figure(figsize=(5,2), dpi=100, facecolor="None",
            constrained_layout=True)
    spectra = graph.subplots(1,1, subplot_kw={"facecolor": (1, 1, 1, 0)})
    canvas = FigureCanvas(graph)
    canvas.setStyleSheet("background-color:transparent;")
    canvas.setAutoFillBackground(False)
    spectra.set_xlabel("Time (min)", )
    spectra.set_ylabel("Pressure (bar)")
    #spectra.set_xticks(np.linspace(100,200,6))
    #spectra.set_xlim(0,40)
    spectra.set_ylim(-1,50)
    #spectra[0].set_yticks(np.linspace(1,8,8))
        #spectrum.set_ylim(0.9,100000)
    spectra.minorticks_on()
    spectra.grid(True)
    spectra.grid(True, 'minor', linewidth='0.2')


    main_window = QtWidgets.QMainWindow(windowTitle="VSWR view")
    for name, style in zip(files, styles):
        datas = load_file(pathprefix+name)
        populate(datas, spectra, style)
    spectra.legend(loc=2)
    spectra.axvline(5, color="#FF000088", linestyle=":")
    spectra.axvline(10, color="#FF000088", linestyle=":")
    spectra.add_patch(rect((-10,-10),15,80,fc="#FFCC0022"))
    spectra.add_patch(rect((5,-10),5,80,fc="#0000FF10"))
    spectra.add_patch(rect((10,-10),12,80,fc="#FFCC0022"))
    spectra.annotate('$100\%\ MeOH$', (0,5), color="#884400FF")
    spectra.annotate('$100\%\ H_2O$', (6.25,5), color="#0000AAFF")
    spectra.annotate('$100\%\ MeOH$', (11.25,5), color="#884400FF")

    graph.savefig(outname+".jpg", dpi=300)
    graph.savefig(outname+".png", dpi=300)
    pixmap=QtGui.QPixmap(outname+".png")
    label = QtWidgets.QLabel(alignment=QtCore.Qt.AlignCenter)
    main_window.setCentralWidget(label)
    main_window.resizeEvent = lambda x: label.setPixmap(
            pixmap.scaled(x.size(), QtCore.Qt.KeepAspectRatio,
                          QtCore.Qt.SmoothTransformation))
    main_window.show()
    sys.exit(app.exec_())
    return


if __name__ == "__main__":
    index = "b)"
    pathprefix = ''
    files = ['watertest.txt']
    styles = [{'color': (0.3, 0, 0.5, 1), 'ls': '-', 'label': "mixing pressure"}]
    outname = 'mixing_chart'
    main(pathprefix, files, styles, outname, index)
