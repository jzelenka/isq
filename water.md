# Measuring samples in water with ISQ
Water is a bit problematic solvent for ISQ but measurements in it can be done! It only takes a bit longer and you will need to use different procedure!

### How to measure water samples?
 * Do not forget to book some extra time
 * Instead of the normal procedure you will use the procedure for water.

<img src="https://gitlab.science.ru.nl/jzelenka/isq/-/raw/master/waterpics/templates_comparison.png"  width="1000" >

First and last line of the water template flushes the system to the water and then from the water to generally more accepted solvent - methanol.

### Why is water special?
Water mixes exothermically with other solvents like methanol or acetonitrile. To prove my point this is how it looks when you are going from methanol to water and back:

<img src="https://gitlab.science.ru.nl/jzelenka/isq/-/raw/master/waterpics/mixing_chart.png"  width="700" >

Flow was set to **0.5 ml/min**, water was pumped from 5th minute. You see that the pressure raises sharply around 6th minute (0.5 ml dead volume) and then it slowly drops in course of approximately 5 minutes (2.5 ml volume before flushing MeOH). The same applies for the change back from water to methanol. The pressures are lower for the normal operating flow of 0.2 ml/min.

Whereas compounds soluble in methanol are also soluble in acetonitrile and vice-versa, analytes dissolved in water can precipitate when they are introduced to other solvent. From the abovementioned graph it is apparent that it takes approximately 2.5 ml of solvent to flush the system.
