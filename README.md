# ISQ Operation Guide
If you are new to ISQ and need access to it, please read the following guide.

### How to operate the machine
I've made a video. Its not perfect, but most of the things are covered. Bottlenecks of this video-guide are listed below. Once stable operation procedure will be developed, fixed video will be recorded.

#### Video (youtube)
[![IMAGE ALT TEXT](http://img.youtube.com/vi/M8Ol5ormuOg/0.jpg)](http://www.youtube.com/watch?v=M8Ol5ormuOg "ISQ operation guide")

#### Notes (FAQ)
 * PAY SPECIAL ATTENTION !! DO NOT SET BLANK AS TYPE:BLANK, keep the type as UNKNOWN (as shown in the video).
 * Sample tray is dispayed in the video in the photo inset.
 * Sample tray can be moved by hand when not injecting.
 * Mass-range is predefined to full operational range
 * keep attention to setting proper method - correct solvent and correct mode (positive/negative)
 * Stay away of pollutants!!
 * [take special measures when working with water](water.md)

### How to gain access
 * Contact your MS tutor.
 * Watch the video.
 * Measure few times with the MS tutor.
 * Request MS tutor to write me (Jan) and I will grant you access for the booking system.

### Booking system
[Link to the booking system](https://bookings.science.ru.nl/public/portal/index/domain/3/catid/25)

### Known Pollutants
 * polar aprotic solvents (DMF, DMSO, HMPA..)
 * buffers (TEAB ..) and concentrated ionic/easily ionizable compounds (TEA, DIPEA..)
 * electrolytes (TBAF, TBAPF6, ...)
 * molecular sieves, microcrystalline compounds
 * low-grade solvents
 * concentrated samples
 * plastics, plasticizers
 * triphenylphosphine-oxide

### Operation procedure
 * Check if the solvent is there (> 200ml). If not, refill with pre-mix. If pre-mix is gone, contact me or Jaya.
 * Insert your samples + blank.
 * Copy the standard sample template (or [water template for water experiments](water.md)) to your folder and personalize it.
 * Measure the samples.
 * Evaluate the samples.
 * Print / upload the outcome.
 * Check that the machine is clean once you finish.
 * Write a note into the logbook (its next to the machine, you will see notes from previous users there)

### Network storage
Network storage is working. But uploading for network storage is manual (export either .raw, or .pdf and move it to appropriate network storage folder)

Network storage address:
**\\\\msspectra-srv.science.ru.nl\\msspectra\\**

The network storage serves just as a transfer station and old data will be periodically delted from this storage. It should substitute printing and USB stick. It is not intended as an ultimate research data storage!!

### When something goes wrong
Contact your MS tutor! If MS tutor is not around contact some other MS tutor.

### List of MS Tutors
|Name                       |        Group           |
| -----------               | -----------            |
|None                       |       Eurostars        |
|Pals, M.J. (Mathijs)       |       Velema/Huck      |
|Crielaard, S. (Stefan)     |       Velema/Huck      |
|Jona Merx                  |       Boltje/Rutjes    |
|Meeusen, E. (Evy)          |       Boltje/Rutjes    |
|Yvonne Bartels             |       Bonger           |
|Hamstra, D.F.J. (Daan)     |       Bonger           |
|Venrooij, K.R. (Kevin)     |       Bonger           |
|Bruekers, J.P.J. (Jeroen)  |       Nolte            |
|Gavriel, K. (Katerina)     |       Wilson           |
|Haije, R. (Rianne)         |       Huck             |

Your group not in a list? **Become a MS tutor yourself!** (PhD preferred)


### Questions/Misc
 * If something is wrong in this guide feel free to contact me.

### TODO (planned improvements)
 * SOP is still missing
 * Explicitly show the type blank FAQ issue in the video !!


