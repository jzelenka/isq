# Tips & Tricks - ISQ Maintenance

Ideas and routines which may help you with ISQ maintenance.

### Stucked tubing
 * PVG magic: Reverse the flow, mechanical debrits at entrance will simply fall off.
 * Rainbow flush: If something was soluble, it can often be dissolved again. You just need to get the right solvent, why not to try them all?
 * Prolonged flushing with small flows can do a lot.

### What went wrong??
ISQ has a very nice logging system. Look into it. Usually there is a clue.
